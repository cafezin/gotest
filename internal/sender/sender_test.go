package sender_test

import (
	"go-sonarcloud/internal/sender"
	"testing"
)

func TestSender_Send(t *testing.T) {
	s := sender.Sender{}

	wasSent := s.Send("1234")

	if !wasSent {
		t.Fail()
	}
}

func TestSender_SendFail(t *testing.T) {
	s := sender.Sender{}

	wasSent := s.Send("")

	if wasSent {
		t.Fail()
	}
}
