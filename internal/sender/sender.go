package sender

import "strings"

type Send interface {
	Send(message string) bool
}

type Sender struct {
}

func (s Sender) Send(message string) bool {
	if isEmptyString(message) {
		return false
	}
	return true
}

func isEmptyString(message string) bool {
	return len(strings.TrimSpace(message)) <= 0
}
