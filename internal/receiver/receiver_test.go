package receiver_test

import (
	"go-sonarcloud/internal/receiver"
	"testing"
)

func TestReceiver_Receive(t *testing.T) {
	r := receiver.Receiver{}

	m := r.Receive()

	if len(m) == 0 {
		t.Fail()
	}
}
