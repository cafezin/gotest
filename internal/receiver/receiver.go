package receiver

import "strings"

type Receive interface {
	Receive() string
}

type Receiver struct {
}

func (r Receiver) Receive() string {
	return "1234"
}

func isEmptyString(message string) bool {
	return len(strings.TrimSpace(message)) <= 0
}
